import pandas as pd
import rdflib as rl
from rdflib.namespace import RDF, XSD
from rdflib import Namespace, Literal, URIRef
from tqdm import tqdm
import sys

if len(sys.argv) < 2:
    print("Usage: python script.py <path_to_csv_file>")
    sys.exit(1)

file_path = sys.argv[1]

df = pd.read_csv(file_path)
graph = rl.Graph()

saref = Namespace("https://saref.etsi.org/core/v3.1.1/")
graph.bind("sf", saref)
# we need to define unique identifiers for devices and timestamps
ex = Namespace("https://example.org/")
graph.bind("ex", ex)
saref_bldg = Namespace("https://saref.etsi.org/saref4bldg/v1.1.2/")
graph.bind("saref_bldg", saref_bldg)
om = Namespace("http://www.ontology-of-units-of-measure.org/resource/om-2/")
graph.bind("om", om)


# the resulting file was too big, so we introduce some systemacy? basically reduce file size through
# identifying similar patterns (device names, buildings).
def get_device_and_location(col_name):
    parts = col_name.split('_')
    location = parts[2]
    device = '_'.join(parts[3:])
    return location, device


def add_row_to_graph(row, i, columns, graph, defined_devices, defined_buildings):
    # Check if timestamp is null
    if pd.notnull(row['utc_timestamp']):
        timestamp_uri = ex[f'timestamp_{i}']
        # add timestamps
        graph.add((timestamp_uri, RDF.type, saref.Time))
        graph.add((timestamp_uri, saref.hasTimestamp, Literal(row['utc_timestamp'], datatype=XSD.dateTime)))
    else:
        timestamp_uri = None

    # add kw/hour unit measurement
    kw_hour_uri = om["kilowattHour"]
    graph.add((kw_hour_uri, RDF.type, saref.UnitOfMeasure))
    # iterate over devices and add to the graph
    for column in columns:
        if column not in ['utc_timestamp', 'cet_cest_timestamp', 'interpolated']:
            if pd.notnull(row[column]):
                building, device = get_device_and_location(column)
                building_uri = ex[str(building)]
                device_uri = ex[f'{building}_{device}']
                measurement_uri = ex[f'{building}_{device}_{i}']
                if building_uri not in defined_buildings:
                    graph.add((building_uri, RDF.type, saref_bldg.Building))
                    defined_buildings.add(building)
                if device_uri not in defined_devices:
                    graph.add((device_uri, RDF.type, saref.Device))
                    graph.add((device_uri, saref_bldg.isContainedIn, building_uri))
                    defined_devices.add(device)

                graph.add((measurement_uri, RDF.type, saref.Measurement))
                graph.add((measurement_uri, saref.hasValue, Literal(row[column], datatype=XSD.float)))
                if timestamp_uri:
                    graph.add((measurement_uri, saref.hasTimestamp, timestamp_uri))
                graph.add((device_uri, saref.relatesToMeasurement, measurement_uri))
                graph.add((measurement_uri, saref.isMeasuredIn, kw_hour_uri))


# we need to serialize in chunks because it takes ages. Also, this will allow a neety progress bar.
chunk_size = 300
total_chunks = (len(df) + chunk_size - 1) // chunk_size
defined_devices = set()
defined_buildings = set()
with open('graph.ttl', 'w') as f:
    for start in tqdm(range(0, len(df), chunk_size), total=total_chunks, desc="Transforming Data"):

        end = start + chunk_size
        chunk = df.iloc[start:end]
        for i, row in chunk.iterrows():
            add_row_to_graph(row=row,
                             i=i,
                             columns=chunk.columns,
                             graph=graph,
                             defined_devices=defined_devices,
                             defined_buildings=defined_buildings
                             )

        turtle_data = graph.serialize(format='turtle')
        f.write(turtle_data)

        # clear graph for next chunk (to free up memory)
        graph.remove((None, None, None))




